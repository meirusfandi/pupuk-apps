<?php
defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . "libraries/REST_Controller.php";

class Auth extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Authmodel', 'auth');
    }

    public function login_post()
    {
        $username = $this->post('username');
        $password = $this->post('password');

        $condition = array(
            'username' => $username,
            'password' => $password
        );

        $data = $this->auth->login($condition);

        if ($data) {
            $this->response(array(
                'status' => 1,
                'message' => 'Authentication success',
                'data' => $data
            ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => 0,
                'message' => 'Authentication failed!'
            ), REST_Controller::HTTP_UNAUTHORIZED);
        }
    }
}
