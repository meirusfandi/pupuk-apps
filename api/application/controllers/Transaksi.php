<?php
defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . "libraries/REST_Controller.php";

class Transaksi extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Transaksimodel', 'transaksi');
    }

    public function index_get()
    {
        $id = $this->get('id_transaksi');
        $id_user = $this->get('id_pengecer');
        $id_pembayaran = $this->get('id_pembayaran');

        if ($id === null) {
            $data = $this->transaksi->getTransaksi($id_user);
        } else {
            $data = $this->transaksi->getTransaksi($id_user, $id);
            if ($id_pembayaran === null) {
                $data[0]['detail'] = $this->transaksi->getDetailTransaksi($id);
            } else {
                $data[0]['pembayaran'] = $this->transaksi->getPembayaran($id_pembayaran);
                $data[0]['detail'] = $this->transaksi->getDetailTransaksi($id);
            }
        }

        if ($data) {
            $this->response(array(
                'status' => 1,
                'message' => 'Success to get data transaksi',
                'data' => $data
            ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => 0,
                'message' => 'Data not found',
                'data' => $data
            ), REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function distributor_get()
    {
        $id = $this->get('id_transaksi');
        $id_user = $this->get('id_distributor');
        $id_pembayaran = $this->get('id_pembayaran');

        if ($id === null) {
            $data = $this->transaksi->getTransaksiDistributor($id_user);
        } else {
            $data = $this->transaksi->getTransaksiDistributor($id_user, $id);
            if ($id_pembayaran === null) {
                $data[0]['detail'] = $this->transaksi->getDetailTransaksi($id);
            } else {
                $data[0]['pembayaran'] = $this->transaksi->getPembayaran($id_pembayaran);
                $data[0]['detail'] = $this->transaksi->getDetailTransaksi($id);
            }
        }

        if ($data) {
            $this->response(array(
                'status' => 1,
                'message' => 'Success to get data transaksi',
                'data' => $data
            ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => 0,
                'message' => 'Data not found',
                'data' => $data
            ), REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function distributor_put()
    {
        $idTransaksi = $this->put('id_transaksi');
        $idDistributor = $this->put('id_distributor');
        $status = $this->put('update_status');

        $count = $this->transaksi->updateStatusTransaksi($idDistributor, $idTransaksi, $status);
        if ($count > 0) {
            $this->response(array(
                'status' => 1,
                'message' => 'Update data was Success!',
            ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => 0,
                'message' => 'Update data was Failed!',
            ), REST_Controller::HTTP_OK);
        }
    }

    public function admin_get()
    {
        $id = $this->get('id_transaksi');
        $id_user = $this->get('id_pengecer');

        if ($id === null) {
            $data = $this->transaksi->getTransaksi($id_user);
        } else {
            $data = $this->transaksi->getTransaksi($id_user, $id);
            $data[0]['detail'] = $this->transaksi->getDetailTransaksi($id);
        }

        if ($data) {
            $this->response(array(
                'status' => 1,
                'message' => 'Success to get data transaksi',
                'data' => $data
            ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => 0,
                'message' => 'Data not found'
            ), REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function index_post()
    {
        $id_pengecer = $this->input->post("id_pengecer");
        $id_dist = $this->input->post("id_dist");
        $jumlah_npk = $this->input->post('jumlah_npk');
        $jumlah_urea = $this->input->post('jumlah_urea');
        $jumlah_organik = $this->input->post('jumlah_organik');

        $product = $this->transaksi->getProductDist($id_dist);

        $price_urea = 0;
        $price_organik = 0;
        $price_npk = 0;
        foreach ($product as $prod) {
            if ($prod['nama_pupuk'] == "UREA") {
                $price_urea = $prod['harga'] ?? 0;
            }
            if ($prod['nama_pupuk'] == "NPK") {
                $price_npk = $prod['harga'] ?? 0;
            }
            if ($prod['nama_pupuk'] == "ORGANIK") {
                $price_organik = $prod['harga'] ?? 0;
            }
        }

        $total_harga = ($jumlah_npk * $price_npk) + ($jumlah_organik * $price_organik) + ($jumlah_urea * $price_urea);
        $data = [
            'do' => "transaction" . date("YmdHis"),
            'tanggal' => date('Y-m-d H:i:s'),
            'total_harga' => $total_harga,
            'id_status' => 3,
            'id_distributor' => $id_dist,
            'id_pengecer' => $id_pengecer,
        ];

        $detail = array();
        $npk = [
            'nama_pupuk' => 'NPK',
            'jumlah' => $jumlah_npk,
            'harga' => $price_npk,
            'subtotal' => $jumlah_npk * $price_npk
        ];

        $organik = [
            'nama_pupuk' => 'ORGANIK',
            'jumlah' => $jumlah_organik,
            'harga' => $price_organik,
            'subtotal' => $jumlah_organik * $price_organik
        ];

        $urea = [
            'nama_pupuk' => 'UREA',
            'jumlah' => $jumlah_urea,
            'harga' => $price_urea,
            'subtotal' => $jumlah_urea * $price_urea
        ];

        array_push($detail, $npk);
        array_push($detail, $urea);
        array_push($detail, $organik);

        $id = $this->transaksi->addTransaksi($data);
        if ($id > 0) {
            if ($this->transaksi->addDetailTransaksi($id, $detail)) {
                $data['id_transaksi'] = $id;
                $result = $this->transaksi->getTransaksi($id_pengecer, $id);
                $this->response(array(
                    'status' => 1,
                    'message' => 'Success to created new transaksi!',
                    'data' => $result
                ), REST_Controller::HTTP_CREATED);
            } else {
                $this->transaksi->deleteTransaksi($id);
                $this->response(array(
                    'status' => 0,
                    'message' => 'Add transaction has been failed!'
                ), REST_Controller::HTTP_BAD_REQUEST);
            }
        } else {
            $this->response(array(
                'status' => 0,
                'message' => 'Add transaction has been failed!'
            ), REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function upload_pembayaran_post()
    {
        $idPengecer = $this->post("id_pengecer");
        $idTransaksi = $this->post("id_transaksi");

        if ($_FILES['bukti_bayar']['name'] != "") {
            $config['upload_path'] = './assets/uploads/buktibayar/';
            $config['allowed_types'] =     'gif|jpg|png|jpeg|pdf';
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('bukti_bayar')) {
                $error = array('error' => $this->upload->display_errors());
                $this->response(array(
                    'status' => 0,
                    'message' => 'Gagal melakukan upload bukti pembayaran.',
                    'data' => $error
                ), REST_Controller::HTTP_BAD_REQUEST);
            } else {
                $upload_data = $this->upload->data();
                $image_name = $upload_data['file_name'];
            }
            $id = $this->transaksi->uploadPembayaran($image_name);
            if ($id > 0) {
                $data = $this->transaksi->updatePembayaran($idPengecer, $idTransaksi, '5', $id);
                $this->response(array(
                    'status' => 1,
                    'message' => 'Upload bukti pembayaran berhasil dilakukan.',
                    'data' => $data
                ), REST_Controller::HTTP_BAD_REQUEST);
            } else {
                $this->response(array(
                    'status' => 0,
                    'message' => 'Gagal melakukan upload bukti pembayaran.',
                    'data' => $error
                ), REST_Controller::HTTP_BAD_REQUEST);
            }
        } else {
            $this->response(array(
                'status' => 0,
                'message' => 'Terjadi kesalahan saat mengupload bukti pembayaran.'
            ), REST_Controller::HTTP_BAD_REQUEST);
        }
    }
}
