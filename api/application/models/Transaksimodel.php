<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Transaksimodel extends CI_Model
{

    public function getProductDist($distId)
    {
        $this->db->select("*");
        $this->db->from('dist_has_pupuk');
        $this->db->where(array('id_distributor' => $distId));
        $query = $this->db->get();

        return $query->result_array();
    }

    public function getPembayaran($idPembayaran = null)
    {
        $this->db->select('*');
        $this->db->from('pembayaran');
        $this->db->where(array('id_pembayaran' => $idPembayaran));
        return $this->db->get()->result_array();
    }

    public function getTransaksi($id_user = null, $id = null)
    {
        if ($id === null) {
            $this->db->select('*');
            $this->db->from('transaksi');
            $this->db->where(array('id_pengecer' => $id_user));
            $this->db->order_by('tanggal', 'desc');
            $query = $this->db->get();

            return $query->result_array();
        } else {
            $this->db->distinct();
            $this->db->select('trx.id_transaksi, trx.do, trx.tanggal, trx.total_harga, dist.nama_distributor, dist.rekening, pcr.nama_pengecer, pcr.alamat, status.status_transaksi');
            $this->db->from('transaksi trx');
            $this->db->join('distributor dist', 'dist.id_login = trx.id_distributor');
            $this->db->join('pengecer pcr', 'pcr.id_login = trx.id_pengecer');
            $this->db->join('status', 'status.id_status = trx.id_status');
            $this->db->where(array('trx.id_transaksi' => $id, 'trx.id_pengecer' => $id_user));
            $this->db->where(array('pcr.id_login' => $id_user));
            $this->db->order_by('tanggal', 'desc');
            $query = $this->db->get();

            return $query->result_array();
        }
    }

    public function getTransaksiDistributor($id_dist = null, $id = null)
    {
        if ($id === null) {
            $this->db->select('*');
            $this->db->from('transaksi');
            $this->db->where(array('transaksi.id_distributor' => $id_dist));
            $query = $this->db->get();

            return $query->result_array();
        } else {
            $this->db->distinct();
            $this->db->select('trx.id_transaksi, trx.do, trx.tanggal, trx.total_harga, dist.nama_distributor, dist.rekening, pcr.nama_pengecer, pcr.alamat, status.status_transaksi');
            $this->db->from('transaksi trx');
            $this->db->join('distributor dist', 'dist.id_login = trx.id_distributor');
            $this->db->join('pengecer pcr', 'pcr.id_login = trx.id_pengecer');
            $this->db->join('status', 'status.id_status = trx.id_status');
            $this->db->where(array('trx.id_transaksi' => $id, 'trx.id_distributor' => $id_dist));
            $this->db->where(array('dist.id_login' => $id_dist));
            $query = $this->db->get();

            return $query->result_array();
        }
    }

    public function getDetailTransaksi($id)
    {
        $this->db->select('*');
        $this->db->from('transaksi_detail');
        $this->db->where(array('id_transaksi' => $id));
        $query = $this->db->get();

        return $query->result_array();
    }

    public function addTransaksi($data)
    {
        $this->db->insert('transaksi', $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    public function addDetailTransaksi($id, $data)
    {
        foreach ($data as $key) {
            $key['id_transaksi'] = $id;
            $this->db->insert('transaksi_detail', $key);
        }

        return $this->db->affected_rows();
    }

    public function uploadPembayaran($filename)
    {
        $data = array(
            'bukti' => $filename
        );
        $this->db->insert('pembayaran', $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    public function updatePembayaran($idPengecer, $idTransaksi, $status, $idPembayaran)
    {
        $this->db->set(array('id_status' => $status, 'id_pembayaran' => $idPembayaran));
        $this->db->where(array('id_transaksi' => $idTransaksi, 'id_pengecer' => $idPengecer));
        $this->db->update('transaksi');
        return $this->db->affected_rows();
    }


    public function updateStatusTransaksi($idDistributor, $idTransaksi, $status)
    {
        $this->db->set('id_status', $status);
        $this->db->where(array('id_transaksi' => $idTransaksi, 'id_distributor' => $idDistributor));
        $this->db->update('transaksi');
        return $this->db->affected_rows();
    }

    public function deleteTransaksi($id)
    {
        $this->db->where(['id_transaksi' => $id]);
        $this->db->delete('transaksi');

        return $this->db->affected_rows();
    }

    public function deleteDetailTransaksi($id)
    {
        $this->db->where(['id_transaksi' => $id]);
        $this->db->delete('transaksi_detail');

        return $this->db->affected_rows();
    }
}
