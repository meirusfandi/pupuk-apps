<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Authmodel extends CI_Model
{

    public function login($condition)
    {
        $this->db->select('*');
        $this->db->where($condition);
        $this->db->from('login');
        $query = $this->db->get();

        return $query->row();
    }
}
