<?php
session_start();
date_default_timezone_set('Asia/Jakarta');
require_once('inc/config.php');
require_once('inc/publicfunc.php');

if(isset($_GET['do'])){
	if ($_GET['do']=='logout') {
		session_destroy();
	}
}

if (isset($_POST['login'])){
	$user=strip_word_html(strtoupper($_POST['username']));
	$pass=strip_word_html(strtoupper($_POST['password']));
	$sql=mysqli_query($con, "SELECT * FROM login WHERE username='$user' AND password='$pass'");
	$row=mysqli_fetch_array($sql);
	if (mysqli_num_rows($sql) > 0){
		$_SESSION['user_pupuk']=$user;
		$_SESSION['id_login']=$row['id_login'];
		$_SESSION['timestamp_pupuk']=time();
		_direct("index.php");
	} else {
		$pesan="Login gagal. Coba lagi.";
		$warna="red";
	}
}

if(isset($_SESSION['user_pupuk'])) _direct("index.php");
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>THE PUPUK | LOG IN</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="vendors/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="vendors/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="vendors/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="assets/css/AdminLTE.min.css">

</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="#"><b>THE PUPUK</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
	<?php
		if (isset($pesan)){
			echo '<center><span class="badge bg-' .$warna. '">' .$pesan. '</span></center><br/>';
		}
	?>
    <form method="post">
	  <input type="hidden" name="login" value="true">
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="username" placeholder="Username" autocomplete="off" required>
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" name="password" placeholder="Password" autocomplete="off" required>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-12 text-center">
			<button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
</body>
</html>
