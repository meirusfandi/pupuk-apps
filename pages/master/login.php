<?php
if (isset($tambah_login_post)){
	$sql = "INSERT INTO login VALUES(null,'$username','$password','$status', null)";
	$q = mysqli_query($con,$sql);
	if ($q){
		_buat_pesan("Input Berhasil","green");
	} else {
		_buat_pesan("Input Gagal","red");
	}
	_direct("?mod=master&page=login");
}
if (isset($_GET['del'])){
	$sql = "DELETE FROM login WHERE id_login=" .$_GET['del'];
	$q = mysqli_query($con, $sql);
	if ($q){
		_buat_pesan("Input Berhasil","green");
	} else {
		_buat_pesan("Input Gagal","red");
	}
	_direct("?mod=master&page=login");
}
?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Master Login</h3><br/>
		  <?php
			if (isset($pesan)){
				echo '<span class="badge bg-' .$warna. '">' .$pesan. '</span>';
			}
		  ?>
        </div>
        <div class="box-body">
			<p align="right"><a data-toggle="modal" data-target="#myModal" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Data</a></p>
			
			<table id="table2" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>No.</th>
						<th>Username</th>
						<th>Status</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
<?php
$sql=mysqli_query($con, "SELECT * FROM login");
$x=0;
while ($row=mysqli_fetch_array($sql)){
$x++;
	echo '<tr>
			<td align="center"><div style="min-width:70px">' .$x. '.</div></td>
			<td align="center"><div style="min-width:70px">' .$row['username']. '</div></td>
			<td align="center"><div style="min-width:70px">' .$row['status']. '</div></td>
			<td align="center"><a class="btn btn-primary btn-sm" href="?mod=master&page=login&del=' .$row['id_login']. '"><i class="fa fa-trash"></i> Hapus</a></td>
		</tr>';
}
?>					
				</tbody>
			</table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
 <!-- modal input -->
<div id="myModal" class="modal modal-default fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><div style="min-width:50px">&times;</div></button>
				<h4 class="modal-title">Tambah Data Login</h4>
			</div>
			<div class="modal-body">				
				<form action="" method="post">
					<input type="hidden" name="tambah_login_post" value="true">
					<div class="col-md-4">Username</div>
					<div class="col-md-8">
						<input class="form-control" type="text" name="username" maxlength="255" required>
					</div>
					<div class="col-md-4">Password</div>
					<div class="col-md-8">
						<input class="form-control" type="text" name="password" maxlength="255" required>
					</div>
					<div class="clearfix"></div>
					<div class="col-md-4">Status</div>
					<div class="col-md-8">	
						<select class="form-control select" id="select_status" name="status" required>
							<option value="" disabled selected>Pilih Status</option>
							<option value="DISTRIBUTOR">DISTRIBUTOR</option>
							<option value="PENGAWAS">PENGAWAS</option>
							<option value="PENGECER">PENGECER</option>
						</select>
					</div>
					<div class="clearfix"></div>
					
			</div>
			<div class="modal-footer">
				<input type="submit" class="btn btn-warning" value="Simpan">
			</div>
				</form>
		</div>
	</div>
</div>



<script>
$(document).ready(function(){
	
});
</script>