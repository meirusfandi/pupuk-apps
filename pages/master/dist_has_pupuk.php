<?php
if (isset($tambah_dist_pupuk_post)){
	$sql = "INSERT INTO dist_has_pupuk VALUES(null,$distributor,'$pupuk',$harga)";
	$q = mysqli_query($con,$sql);
	if ($q){
		_buat_pesan("Input Berhasil","green");
	} else {
		_buat_pesan("Input Gagal","red");
	}
	_direct("?mod=master&page=dist_has_pupuk");
}
if (isset($_GET['del'])){
	$sql = "DELETE FROM dist_has_pupuk WHERE id_dist_has_pupuk=" .$_GET['del'];
	$q = mysqli_query($con, $sql);
	if ($q){
		_buat_pesan("Input Berhasil","green");
	} else {
		_buat_pesan("Input Gagal","red");
	}
	_direct("?mod=master&page=dist_has_pupuk");
}
?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Master Pupuk</h3><br/>
		  <?php
			if (isset($pesan)){
				echo '<span class="badge bg-' .$warna. '">' .$pesan. '</span>';
			}
		  ?>
        </div>
        <div class="box-body">
			<p align="right"><a data-toggle="modal" data-target="#myModal" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Data</a></p>
			
			<table id="table2" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>No.</th>
						<th>Nama Distributor</th>
						<th>Nama Pupuk</th>
						<th>Harga</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
<?php
$sql=mysqli_query($con, "SELECT * FROM dist_has_pupuk INNER JOIN distributor ON (dist_has_pupuk.id_distributor = distributor.id_distributor)");
$x=0;
while ($row=mysqli_fetch_array($sql)){
$x++;
	echo '<tr>
			<td align="center"><div style="min-width:70px">' .$x. '.</div></td>
			<td align="center"><div style="min-width:70px">' .$row['nama_distributor']. '</div></td>
			<td align="center"><div style="min-width:70px">' .$row['nama_pupuk']. '</div></td>
			<td align="center"><div style="min-width:70px">' .$row['harga']. '</div></td>
			<td align="center"><a class="btn btn-primary btn-sm" href="?mod=master&page=dist_has_pupuk&del=' .$row['id_dist_has_pupuk']. '"><i class="fa fa-trash"></i> Hapus</a></td>
		</tr>';
}
?>					
				</tbody>
			</table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
 <!-- modal input -->
<div id="myModal" class="modal modal-default fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><div style="min-width:50px">&times;</div></button>
				<h4 class="modal-title">Tambah Data Dist Pupuk</h4>
			</div>
			<div class="modal-body">				
				<form action="" method="post">
					<input type="hidden" name="tambah_dist_pupuk_post" value="true">
					<div class="col-md-4">Distributor</div>
					<div class="col-md-8">
						<select class="form-control" id="distributor" name="distributor" required>
						<option value="" disabled selected>Pilih Distributor</option>
						<?php
							$sql=mysqli_query($con, "SELECT * FROM distributor");
							while ($row=mysqli_fetch_array($sql)){
								echo '<option value="' .$row['id_distributor']. '">' .$row['nama_distributor']. '</option>';
							}
						?>
						</select>
					</div>
					<div class="col-md-4">Nama Pupuk</div>
					<div class="col-md-8">
						<input class="form-control" id="pupuk" name="pupuk" required>
					</div>
					<div class="col-md-4">Harga Pupuk</div>
					<div class="col-md-8">
						<input type="number" class="form-control" id="harga" name="harga" required>
					</div>
					<div class="clearfix"></div>
					
			</div>
			<div class="modal-footer">
				<input type="submit" class="btn btn-warning" value="Simpan">
			</div>
				</form>
		</div>
	</div>
</div>
<script>
$( document ).ready(function() {
    
});
</script>
