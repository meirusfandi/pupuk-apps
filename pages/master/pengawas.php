<?php
if (isset($tambah_pengawas_post)){
	$sql = "INSERT INTO pengawas VALUES(null,'$id_login','$nama','$jabatan')";
	$q = mysqli_query($con,$sql);
	if ($q){
		_buat_pesan("Input Berhasil","green");
	} else {
		_buat_pesan("Input Gagal","red");
	}
	_direct("?mod=master&page=pengawas");
}
if (isset($_GET['del'])){
	$sql = "DELETE FROM pengawas WHERE id_pengawas=" .$_GET['del'];
	$q = mysqli_query($con, $sql);
	if ($q){
		_buat_pesan("Input Berhasil","green");
	} else {
		_buat_pesan("Input Gagal","red");
	}
	_direct("?mod=master&page=pengawas");
}
?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Master Pengawas</h3><br/>
		  <?php
			if (isset($pesan)){
				echo '<span class="badge bg-' .$warna. '">' .$pesan. '</span>';
			}
		  ?>
        </div>
        <div class="box-body">
			<p align="right"><a data-toggle="modal" data-target="#myModal" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Data</a></p>
			
			<table id="table2" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>No.</th>
						<th>ID Login</th>
						<th>Nama</th>
						<th>Jabatan</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
<?php
$sql=mysqli_query($con, "SELECT * FROM pengawas");
$x=0;
while ($row=mysqli_fetch_array($sql)){
$x++;
	echo '<tr>
			<td align="center"><div style="min-width:70px">' .$x. '.</div></td>
			<td align="center"><div style="min-width:70px">' .$row['id_login']. '</div></td>
			<td align="center"><div style="min-width:70px">' .$row['nama_pengawas']. '</div></td>
			<td align="center"><div style="min-width:70px">' .$row['jabatan']. '</div></td>
			<td align="center"><a class="btn btn-primary btn-sm" href="?mod=master&page=pengawas&del=' .$row['id_pengawas']. '"><i class="fa fa-trash"></i> Hapus</a></td>
		</tr>';
}
?>					
				</tbody>
			</table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
 <!-- modal input -->
<div id="myModal" class="modal modal-default fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><div style="min-width:50px">&times;</div></button>
				<h4 class="modal-title">Tambah Data Pengawas</h4>
			</div>
			<div class="modal-body">				
				<form action="" method="post">
					<input type="hidden" name="tambah_pengawas_post" value="true">
					<div class="col-md-4">Username</div>
					<div class="col-md-8">
						<select class="form-control" name="id_login" required>
						<?php
							$sql=mysqli_query($con, "SELECT * FROM login WHERE status='PENGAWAS'");
							while ($row=mysqli_fetch_array($sql)){
								echo '<option value="' .$row['id_login']. '">' .$row['username']. '</option>';
							}
						?>
						</select>
					</div>
					<div class="col-md-4">Nama</div>
					<div class="col-md-8">
						<input class="form-control" type="text" name="nama" maxlength="255" required>
					</div>
					<div class="col-md-4">Jabatan</div>
					<div class="col-md-8">
						<input class="form-control" type="text" name="jabatan" maxlength="255" required>
					</div>
					
					<div class="clearfix"></div>
					
			</div>
			<div class="modal-footer">
				<input type="submit" class="btn btn-warning" value="Simpan">
			</div>
				</form>
		</div>
	</div>
</div>



<script>
$(document).ready(function(){
	
});
</script>