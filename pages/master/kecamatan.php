<?php
if (isset($tambah_kecamatan_post)){
	$sql = "INSERT INTO kecamatan VALUES(null,$id_kabupaten,'$nama')";
	$q = mysqli_query($con,$sql);
	if ($q){
		_buat_pesan("Input Berhasil","green");
	} else {
		_buat_pesan("Input Gagal","red");
	}
	_direct("?mod=master&page=kecamatan");
}
if (isset($_GET['del'])){
	$sql = "DELETE FROM kecamatan WHERE id_kecamatan=" .$_GET['del'];
	$q = mysqli_query($con, $sql);
	if ($q){
		_buat_pesan("Input Berhasil","green");
	} else {
		_buat_pesan("Input Gagal","red");
	}
	_direct("?mod=master&page=kecamatan");
}
?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Master Kecamatan</h3><br/>
		  <?php
			if (isset($pesan)){
				echo '<span class="badge bg-' .$warna. '">' .$pesan. '</span>';
			}
		  ?>
        </div>
        <div class="box-body">
			<p align="right"><a data-toggle="modal" data-target="#myModal" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Data</a></p>
			
			<table id="table2" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>No.</th>
						<th>Nama Provinsi</th>
						<th>Nama Kabupaten</th>
						<th>Nama Kecamatan</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
<?php
$sql=mysqli_query($con, "SELECT * FROM provinsi INNER JOIN kabupaten ON (provinsi.id_provinsi = kabupaten.id_provinsi) INNER JOIN kecamatan ON (kabupaten.id_kabupaten = kecamatan.id_kabupaten)");
$x=0;
while ($row=mysqli_fetch_array($sql)){
$x++;
	echo '<tr>
			<td align="center"><div style="min-width:70px">' .$x. '.</div></td>
			<td align="center"><div style="min-width:70px">' .$row['nama_provinsi']. '</div></td>
			<td align="center"><div style="min-width:70px">' .$row['nama_kabupaten']. '</div></td>
			<td align="center"><div style="min-width:70px">' .$row['nama_kecamatan']. '</div></td>
			<td align="center"><a class="btn btn-primary btn-sm" href="?mod=master&page=kecamatan&del=' .$row['id_kecamatan']. '"><i class="fa fa-trash"></i> Hapus</a></td>
		</tr>';
}
?>					
				</tbody>
			</table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
 <!-- modal input -->
<div id="myModal" class="modal modal-default fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><div style="min-width:50px">&times;</div></button>
				<h4 class="modal-title">Tambah Data Kecamatan</h4>
			</div>
			<div class="modal-body">				
				<form action="" method="post">
					<input type="hidden" name="tambah_kecamatan_post" value="true">
					<div class="col-md-4">Nama Kabupaten</div>
					<div class="col-md-8">
						<select class="form-control" name="id_kabupaten" required>
						<?php
							$sql=mysqli_query($con, "SELECT * FROM kabupaten");
							while ($row=mysqli_fetch_array($sql)){
								echo '<option value="' .$row['id_kabupaten']. '">' .$row['nama_kabupaten']. '</option>';
							}
						?>
						</select>
					</div>
					<div class="col-md-4">Nama Kecamatan</div>
					<div class="col-md-8">
						<input class="form-control" type="text" name="nama" maxlength="255" required>
					</div>
					<div class="clearfix"></div>
					
			</div>
			<div class="modal-footer">
				<input type="submit" class="btn btn-warning" value="Simpan">
			</div>
				</form>
		</div>
	</div>
</div>



<script>
$(document).ready(function(){
	
});
</script>