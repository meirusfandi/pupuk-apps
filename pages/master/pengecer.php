<?php
if (isset($tambah_pengecer_post)) {
	$sqlLogin = "UPDATE login SET dist_id = $id_distributor WHERE id_login = $id_login";
	$qLogin = mysqli_query($con, $sqlLogin);
	if ($qLogin) {
		$sql = "INSERT INTO pengecer VALUES(null,'$id_login','$nama','$p_jawab','$alamat','$telpon','$email','$j_kelamin','$id_kecamatan')";
		$q = mysqli_query($con, $sql);
		if ($q) {
			_buat_pesan("Input Berhasil", "green");
		} else {
			_buat_pesan("Input Gagal", "red");
		}
		_direct("?mod=master&page=pengecer");
	} else {
		_buat_pesan("Input Gagal ", "red");
	}
}
if (isset($_GET['del'])) {
	$sql = "DELETE FROM pengecer WHERE id_pengecer=" . $_GET['del'];
	$q = mysqli_query($con, $sql);
	if ($q) {
		_buat_pesan("Input Berhasil", "green");
	} else {
		_buat_pesan("Input Gagal", "red");
	}
	_direct("?mod=master&page=pengecer");
}
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Main content -->
	<section class="content">

		<!-- Default box -->
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Master Pengecer</h3><br />
				<?php
				if (isset($pesan)) {
					echo '<span class="badge bg-' . $warna . '">' . $pesan . '</span>';
				}
				?>
			</div>
			<div class="box-body">
				<p align="right"><a data-toggle="modal" data-target="#myModal" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Data</a></p>

				<table id="table2" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>No.</th>
							<th>ID Login</th>
							<th>Nama</th>
							<th>P. Jawab</th>
							<th>Alamat</th>
							<th>Telpon</th>
							<th>Email</th>
							<th>J. Kelamin</th>
							<th>ID Kecamatan</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php
						$sql = mysqli_query($con, "SELECT * FROM pengecer");
						$x = 0;
						while ($row = mysqli_fetch_array($sql)) {
							$x++;
							echo '<tr>
			<td align="center"><div style="min-width:70px">' . $x . '.</div></td>
			<td align="center"><div style="min-width:70px">' . $row['id_login'] . '</div></td>
			<td align="center"><div style="min-width:70px">' . $row['nama_pengecer'] . '</div></td>
			<td align="center"><div style="min-width:70px">' . $row['p_jawab'] . '</div></td>
			<td align="center"><div style="min-width:70px">' . $row['alamat'] . '</div></td>
			<td align="center"><div style="min-width:70px">' . $row['telpon'] . '</div></td>
			<td align="center"><div style="min-width:70px">' . $row['email'] . '</div></td>
			<td align="center"><div style="min-width:70px">' . $row['j_kelamin'] . '</div></td>
			<td align="center"><div style="min-width:70px">' . $row['id_kecamatan'] . '</div></td>
			<td align="center"><a class="btn btn-primary btn-sm" href="?mod=master&page=pengecer&del=' . $row['id_pengecer'] . '"><i class="fa fa-trash"></i> Hapus</a></td>
		</tr>';
						}
						?>
					</tbody>
				</table>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->

	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- modal input -->
<div id="myModal" class="modal modal-default fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					<div style="min-width:50px">&times;</div>
				</button>
				<h4 class="modal-title">Tambah Data Pengecer</h4>
			</div>
			<div class="modal-body">
				<form action="" method="post">
					<input type="hidden" name="tambah_pengecer_post" value="true">
					<div class="col-md-4">Username</div>
					<div class="col-md-8">
						<select class="form-control" name="id_login" required>
							<?php
							$sql = mysqli_query($con, "SELECT * FROM login WHERE status='PENGECER' AND dist_id IS NULL");
							while ($row = mysqli_fetch_array($sql)) {
								echo '<option value="' . $row['id_login'] . '">' . $row['username'] . '</option>';
							}
							?>
						</select>
					</div>
					<div class="col-md-4">Distributor</div>
					<div class="col-md-8">
						<select class="form-control" name="id_distributor" required>
							<?php
							$sql = mysqli_query($con, "SELECT * FROM login WHERE status='DISTRIBUTOR'");
							while ($row = mysqli_fetch_array($sql)) {
								echo '<option value="' . $row['id_login'] . '">' . $row['username'] . '</option>';
							}
							?>
						</select>
					</div>
					<div class="col-md-4">Nama</div>
					<div class="col-md-8">
						<input class="form-control" type="text" name="nama" maxlength="255" required>
					</div>
					<div class="col-md-4">Penanggung Jawab</div>
					<div class="col-md-8">
						<input class="form-control" type="text" name="p_jawab" maxlength="255" required>
					</div>
					<div class="col-md-4">Alamat</div>
					<div class="col-md-8">
						<input class="form-control" type="text" name="alamat" maxlength="255" required>
					</div>
					<div class="col-md-4">Telpon</div>
					<div class="col-md-8">
						<input class="form-control" type="text" name="telpon" maxlength="255" required>
					</div>
					<div class="col-md-4">Email</div>
					<div class="col-md-8">
						<input class="form-control" type="text" name="email" maxlength="255" required>
					</div>
					<div class="col-md-4">Jenis Kelamin</div>
					<div class="col-md-8">
						<select class="form-control" name="j_kelamin" required>
							<option value="LAKI-LAKI">LAKI-LAKI</option>
							<option value="PEREMPUAN">PEREMPUAN</option>
						</select>
					</div>
					<div class="col-md-4">Nama Kecamatan</div>
					<div class="col-md-8">
						<select class="form-control" name="id_kecamatan" required>
							<?php
							$sql = mysqli_query($con, "SELECT * FROM kecamatan");
							while ($row = mysqli_fetch_array($sql)) {
								echo '<option value="' . $row['id_kecamatan'] . '">' . $row['nama_kecamatan'] . '</option>';
							}
							?>
						</select>
					</div>
					<div class="clearfix"></div>

			</div>
			<div class="modal-footer">
				<input type="submit" class="btn btn-warning" value="Simpan">
			</div>
			</form>
		</div>
	</div>
</div>



<script>
	$(document).ready(function() {

	});
</script>