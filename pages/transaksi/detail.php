<?php
if (isset($tambah_transaksi_post)){
	$sql = "INSERT INTO pembayaran VALUES(null,'$bukti')";
	$q = mysqli_query($con,$sql);
	$bukti = mysqli_insert_id($con);
	$sql = "INSERT INTO transaksi VALUES(null,'$do','$tanggal','$jumlah_pupuk','$total_harga','$status','$distributor','$pengecer','$pupuk',$bukti)";
	$q = mysqli_query($con,$sql);
	if ($q){
		_buat_pesan("Input Berhasil","green");
	} else {
		_buat_pesan("Input Gagal","red");
	}
	_direct("?mod=transaksi&page=view");
}
if (isset($_GET['del'])){
	$sql = "DELETE FROM transaksi WHERE id_transaksi=" .$_GET['del'];
	$q = mysqli_query($con, $sql);
	if ($q){
		_buat_pesan("Input Berhasil","green");
	} else {
		_buat_pesan("Input Gagal","red");
	}
	_direct("?mod=transaksi&page=view");
}
?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">

      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Detail Transaksi</h3>
        </div>
        <div class="box-body">
			<div class="table-responsive">
			<table class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>No.</th>
						<th>Nama Pupuk</th>
						<th>Jumlah</th>
						<th>Harga</th>
						<th>Total</th>
					</tr>
				</thead>
				<tbody>
<?php
$sql=mysqli_query($con, "SELECT *
FROM
    transaksi_detail
	WHERE id_transaksi=$id");
$x=0;
while ($row=mysqli_fetch_array($sql)){
$x++;
$total=$row['jumlah']*$row['harga'];
	echo '<tr>
			<td>' .$x. '</td>
			<td>' .$row['nama_pupuk']. '</td>
			<td>' .$row['jumlah']. '</td>
			<td>' .$row['harga']. '</td>
			<td>' .$total. '</td>
		</tr>';
}
?>		
				</tbody>
			</table>
			</div>
        </div>
        <!-- /.box-body -->
	  </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<script>
$( document ).ready(function() {
	
});
</script>