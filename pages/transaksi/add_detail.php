<?php
if (isset($tambah_detail_transaksi_post)){
	$sql = "INSERT INTO transaksi_detail VALUES(null,'$id_transaksi','$pupuk','$jumlah','$harga','$subtotal')";
	$q = mysqli_query($con,$sql);
	if ($q){
		_buat_pesan("Input Berhasil","green");
	} else {
		_buat_pesan("Input Gagal","red");
	}
	$sql=mysqli_query($con,"SELECT SUM(subtotal) FROM transaksi_detail WHERE id_transaksi=$id");
	$row = mysqli_fetch_row($sql);
	$sql=mysqli_query($con,"UPDATE transaksi SET total_harga=" .$row[0]. " WHERE id_transaksi=$id");
	_direct("?mod=transaksi&page=add_detail&dist=" .$_GET['dist']. "&id=$id");
}
if (isset($_GET['del'])){
	$sql = "DELETE FROM transaksi_detail WHERE id_transaksi_detail=" .$_GET['del'];
	$q = mysqli_query($con, $sql);
	if ($q){
		_buat_pesan("Input Berhasil","green");
	} else {
		_buat_pesan("Input Gagal","red");
	}
	$sql=mysqli_query($con,"SELECT SUM(subtotal) FROM transaksi_detail WHERE id_transaksi=$id");
	$row = mysqli_fetch_row($sql);
	$sql=mysqli_query($con,"UPDATE transaksvi SET total_harga=" .$row[0]. " WHERE id_transaksi=$id");
	_direct("?mod=transaksi&page=add_detail&dist=" .$_GET['dist']. "&id=$id");
}
?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">

      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Detail Transaksi</h3>
        </div>
        <div class="box-body">
			<p align="right"><a data-toggle="modal" data-target="#myModal" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Data</a></p>
			
			<div class="table-responsive">
			<table class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>No.</th>
						<th>Nama Pupuk</th>
						<th>Jumlah</th>
						<th>Harga</th>
						<th>Total</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
<?php
$sql=mysqli_query($con, "SELECT *
FROM
    transaksi_detail
	WHERE id_transaksi=$id");
$x=0;
while ($row=mysqli_fetch_array($sql)){
$x++;
$total=$row['jumlah']*$row['harga'];
	echo '<tr>
			<td>' .$x. '</td>
			<td>' .$row['nama_pupuk']. '</td>
			<td>' .$row['jumlah']. '</td>
			<td>' .$row['harga']. '</td>
			<td>' .$total. '</td>
			<td align="center">
				<a class="btn btn-primary btn-sm" href="?mod=transaksi&page=add_detail&dist=' .$_GET['dist']. '&id=' .$id. '&del=' .$row['id_transaksi_detail']. '"><i class="fa fa-trash"></i> Hapus</a>
			</td>
		</tr>';
}
?>		
				</tbody>
			</table>
			</div>
        </div>
        <!-- /.box-body -->
	  </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- modal input -->
<div id="myModal" class="modal modal-default fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><div style="min-width:50px">&times;</div></button>
				<h4 class="modal-title">Tambah Data Detail Transaksi</h4>
			</div>
			<div class="modal-body">				
				<form action="" method="post">
					<input type="hidden" name="tambah_detail_transaksi_post" value="true">
					<input type="hidden" name="id_transaksi" value="<?php echo $id ?>">
					<div class="col-md-4">Nama Pupuk</div>
					<div class="col-md-8">
						<select class="form-control" name="pupuk" required>
						<option value="" disabled selected>Pilih Pupuk</option>
						<?php
							$sql=mysqli_query($con, "SELECT * FROM dist_has_pupuk WHERE id_distributor=" .$_GET['dist']);
							while ($row=mysqli_fetch_array($sql)){
								echo '<option value="' .$row['nama_pupuk']. '">' .$row['nama_pupuk']. ' | Rp. ' .$row['harga']. '</option>';
							}
						?>
						</select>
					</div>
					<div class="col-md-4">Jumlah Pupuk</div>
					<div class="col-md-8">
						<input class="form-control" type="number" name="jumlah" required>
					</div>
					<div class="col-md-4">Harga Pupuk</div>
					<div class="col-md-8">
						<input class="form-control" type="number" name="harga" required>
					</div>
					<div class="clearfix"></div>
					
			</div>
			<div class="modal-footer">
				<input type="submit" class="btn btn-warning" value="Simpan">
			</div>
				</form>
		</div>
	</div>
</div>
<script>
$( document ).ready(function() {
	$("#distributor").change(function(){
		id=$("#distributor").val();
		$('#pupuk').load("index.php?mod=transaksi&page=get-pupuk&id=" +id + "&frameless");
	});
});
</script>