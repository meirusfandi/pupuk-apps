<?php
if (isset($tambah_transaksi_post)){
	$sql = "INSERT INTO transaksi VALUES(null,'$do','$tanggal',null,null,'$distributor','$pengecer',null)";
	$q = mysqli_query($con,$sql);
	$idx=mysqli_insert_id($con);
	if ($q){
		_buat_pesan("Input Berhasil","green");
		_direct("?mod=transaksi&page=add_detail&dist=$distributor&id=".$idx);
	} else {
		_buat_pesan("Input Gagal","red");
		_direct("?mod=transaksi&page=view");
	}
}
if (isset($_GET['del'])){
	$sql = "DELETE FROM transaksi WHERE id_transaksi=" .$_GET['del'];
	$q = mysqli_query($con, $sql);
	if ($q){
		_buat_pesan("Input Berhasil","green");
	} else {
		_buat_pesan("Input Gagal","red");
	}
	_direct("?mod=transaksi&page=view");
}
?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">

      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Transaksi</h3>
        </div>
        <div class="box-body">
			<p align="right"><a data-toggle="modal" data-target="#myModal" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Data</a></p>
			
			<div class="table-responsive">
			<table class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>No.</th>
						<th>DO</th>
						<th>Tanggal</th>
						<th>Total Harga</th>
						<th>Status</th>
						<th>Distributor</th>
						<th>Pengecer</th>
						<th>Pembayaran</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
<?php
$jumlah_keluar=0;
$sql=mysqli_query($con, "SELECT *
FROM
    transaksi
    LEFT JOIN status 
        ON (transaksi.id_status = status.id_status)
    LEFT JOIN distributor 
        ON (transaksi.id_distributor = distributor.id_distributor)
    LEFT JOIN pengecer 
        ON (transaksi.id_pengecer = pengecer.id_pengecer)
    LEFT JOIN pembayaran 
        ON (transaksi.id_pembayaran = pembayaran.id_pembayaran)");
$x=0;
while ($row=mysqli_fetch_array($sql)){
$x++;
	echo '<tr>
			<td>' .$x. '</td>
			<td>' .$row['do']. '</td>
			<td>' .date("d-m-Y",strtotime($row['tanggal'])). '</td>
			<td>' .$row['total_harga']. '</td>
			<td>' .$row['status_transaksi']. '</td>
			<td>' .$row['nama_distributor']. '</td>
			<td>' .$row['nama_pengecer']. '</td>
			<td>' .$row['bukti']. '</td>
			<td align="center">
				<a class="btn btn-primary btn-sm" href="?mod=transaksi&page=detail&id=' .$row['id_transaksi']. '"><i class="fa fa-search"></i> Detail</a>
				<a class="btn btn-primary btn-sm" href="?mod=transaksi&page=view&del=' .$row['id_transaksi']. '"><i class="fa fa-trash"></i> Hapus</a>
			</td>
		</tr>';
}
?>		
				</tbody>
			</table>
			</div>
        </div>
        <!-- /.box-body -->
	  </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- modal input -->
<div id="myModal" class="modal modal-default fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><div style="min-width:50px">&times;</div></button>
				<h4 class="modal-title">Tambah Data Transaksi</h4>
			</div>
			<div class="modal-body">				
				<form action="" method="post">
					<input type="hidden" name="tambah_transaksi_post" value="true">
					<input type="hidden" name="tanggal" value="<?php echo date("Y-m-d H:i:s") ?>">
					<div class="col-md-4">DO</div>
					<div class="col-md-8">
						<input class="form-control" type="text" name="do" maxlength="255" required>
					</div>
					<div class="col-md-4">Distributor</div>
					<div class="col-md-8">
						<select class="form-control" id="distributor" name="distributor" required>
						<option value="" disabled selected>Pilih Distributor</option>
						<?php
							$sql=mysqli_query($con, "SELECT * FROM distributor");
							while ($row=mysqli_fetch_array($sql)){
								echo '<option value="' .$row['id_distributor']. '">' .$row['nama_distributor']. '</option>';
							}
						?>
						</select>
					</div>
					<div class="col-md-4">Pengecer</div>
					<div class="col-md-8">
						<select class="form-control" name="pengecer" required>
						<option value="" disabled selected>Pilih Pengecer</option>
						<?php
							$sql=mysqli_query($con, "SELECT * FROM pengecer");
							while ($row=mysqli_fetch_array($sql)){
								echo '<option value="' .$row['id_pengecer']. '">' .$row['nama_pengecer']. '</option>';
							}
						?>
						</select>
					</div>
					<div class="clearfix"></div>
					
			</div>
			<div class="modal-footer">
				<input type="submit" class="btn btn-warning" value="Simpan">
			</div>
				</form>
		</div>
	</div>
</div>
<script>
$( document ).ready(function() {
	$("#distributor").change(function(){
		id=$("#distributor").val();
		$('#pupuk').load("index.php?mod=transaksi&page=get-pupuk&id=" +id + "&frameless");
	});
});
</script>