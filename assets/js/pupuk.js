function format_uang(n) {
	return "Rp " + Number(n).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
}
function format_uang2(n) {
	return Number(n).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
}
function format_angka(n) {
	var reverse = n.toString().split('').reverse().join(''),
		ribuan = reverse.match(/\d{1,3}/g);
		ribuan = ribuan.join('.').split('').reverse().join('');
	return ribuan;
}
$(document).ready(function(){
	$("#table1").DataTable({
		"pageLength": 50,
		"bPaginate": true,
		"bLengthChange": false,
		"scrollX" : false
	});
	$("#table2").DataTable({
		"pageLength": 50,
		"bPaginate": true,
		"bLengthChange": false,
		"scrollX" : true,
		"drawCallback": function( settings ) {
			$(".dataTables_scrollHeadInner").css({"width":"100%"});
			$(".table ").css({"width":"100%"});
			$("#table2").resize();
		}
	});
	$("#table3").DataTable({
		"order":[],
		"pageLength": 50,
		"bPaginate": true,
		"bLengthChange": false,
		"scrollX" : true,
		"drawCallback": function( settings ) {
			$(".dataTables_scrollHeadInner").css({"width":"100%"});
			$(".table ").css({"width":"100%"});
			$("#table3").resize();
		}
	});
	$(".table2").DataTable({
		"pageLength": 50,
		"bPaginate": true,
		"bLengthChange": false,
		"scrollX" : true,
		"drawCallback": function( settings ) {
			$(".dataTables_scrollHeadInner").css({"width":"100%"});
			$(".table ").css({"width":"100%"});
			$("#table2").resize();
		}
	});
	
	$(window).bind('resize', function () {
		clearTimeout(this.id);
		this.id = setTimeout(doResize, 500);
	});
	function doResize(){
		$("#table2").resize();
		$("#table_belum_tagih").resize();
		$("#table_lunas").resize();
	}
	$('.modal').on('hidden.bs.modal', function(e){
		$(".select2").val($(".select2 option:first-child").val()).trigger('change');
		$("input[type='text']").val('');
		$("input[type='number']").val('');
		$("input[type='tel']").val('');
		$(".select").val('');
		$(".tagsinput .tag").remove();
		$("#tagsinput input").val();	
	});
	$(".select2").select2({
		placeholderOption: "first",
		width: '100%'
	});
	$(document).on('focus', ':input', function() {
		$(this).attr('autocomplete', 'off');
	});
});