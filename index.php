<?php
session_start();
date_default_timezone_set('Asia/Jakarta');
require_once('inc/config.php');
require_once('inc/publicfunc.php');
require_once('inc/check.php');

if (isset($_POST['tambah_login_post'])){
	$tambah_login_post=true;
	$username=$_POST['username'];
	$password=$_POST['password'];
	$status=$_POST['status'];
}
if (isset($_POST['tambah_provinsi_post'])){
	$tambah_provinsi_post=true;
	$nama=$_POST['nama'];
}

if (isset($_POST['tambah_kabupaten_post'])){
	$tambah_kabupaten_post=true;
	$id_provinsi=$_POST['id_provinsi'];
	$nama=$_POST['nama'];
}

if (isset($_POST['tambah_kecamatan_post'])){
	$tambah_kecamatan_post=true;
	$id_kabupaten=$_POST['id_kabupaten'];
	$nama=$_POST['nama'];
}

if (isset($_POST['tambah_distributor_post'])){
	$tambah_distributor_post=true;
	$id_login=$_POST['id_login'];
	$nama=$_POST['nama'];
	$p_jawab=$_POST['p_jawab'];
	$alamat=$_POST['alamat'];
	$telpon=$_POST['telpon'];
	$email=$_POST['email'];
	$rekening=$_POST['rekening'];
	$j_kelamin=$_POST['j_kelamin'];
	$stok=$_POST['stok'];
	$id_kabupaten=$_POST['id_kabupaten'];
}

if (isset($_POST['tambah_pengecer_post'])){
	$tambah_pengecer_post=true;
	$id_login=$_POST['id_login'];
	$id_distributor=$_POST['id_distributor'];
	$nama=$_POST['nama'];
	$p_jawab=$_POST['p_jawab'];
	$alamat=$_POST['alamat'];
	$telpon=$_POST['telpon'];
	$email=$_POST['email'];
	$j_kelamin=$_POST['j_kelamin'];
	$id_kecamatan=$_POST['id_kecamatan'];
}

if (isset($_POST['tambah_pengawas_post'])){
	$tambah_pengawas_post=true;
	$id_login=$_POST['id_login'];
	$nama=$_POST['nama'];
	$jabatan=$_POST['jabatan'];
}

if (isset($_POST['tambah_pupuk_post'])){
	$tambah_pupuk_post=true;
	$nama=$_POST['nama'];
}

if (isset($_POST['tambah_dist_pupuk_post'])){
	$tambah_dist_pupuk_post=true;
	$distributor=$_POST['distributor'];
	$pupuk=$_POST['pupuk'];
	$harga=$_POST['harga'];
}

if (isset($_POST['tambah_status_post'])){
	$tambah_status_post=true;
	$status=$_POST['status'];
}

if (isset($_POST['tambah_transaksi_post'])){
	$tambah_transaksi_post=true;
	$do=$_POST['do'];
	$tanggal=date("Y-m-d H:i:s");
	$distributor=$_POST['distributor'];
	$pengecer=$_POST['pengecer'];
}

if (isset($_POST['tambah_detail_transaksi_post'])){
	$tambah_detail_transaksi_post=true;
	$id_transaksi=$_POST['id_transaksi'];
	$pupuk=$_POST['pupuk'];
	$jumlah=$_POST['jumlah'];
	$harga=$_POST['harga'];
	$subtotal=$jumlah*$harga;
}

if (isset($_GET['id'])){
	$id=$_GET['id'];
}

(isset($_GET['frameless']) ? $frameless=true : $frameless=false);

if(isset($_GET['page'])){
	$mod = $_GET['mod'];
	$page = $_GET['page'];
} else {
	$mod="dashboard";
	$page="dashboard";
}
if (isset($_SESSION['pesan'])){
	$pesan=$_SESSION['pesan'];
	$warna=$_SESSION['warna'];
	unset($_SESSION['pesan']);
	unset($_SESSION['warna']);
}

include "inc/header.php";
if (! $frameless) include "inc/top.php";
if (! $frameless) include "inc/sidebar.php";

include "pages/" .$mod. "/" .$page. ".php";
if (! $frameless) include "inc/footer.php";
