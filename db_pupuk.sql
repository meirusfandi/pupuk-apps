CREATE TABLE `dist_has_pupuk` (
  `id_dist_has_pupuk` int(11) NOT NULL AUTO_INCREMENT,
  `id_distributor` int(11) DEFAULT NULL,
  `nama_pupuk` varchar(255) DEFAULT NULL,
  `harga` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_dist_has_pupuk`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

insert  into `dist_has_pupuk`(`id_dist_has_pupuk`,`id_distributor`,`nama_pupuk`,`harga`) values 
(5,1,'UREA',100000),
(6,1,'NPK',100000),
(7,1,'ORGANIK',100000);

CREATE TABLE `distributor` (
  `id_distributor` int(11) NOT NULL AUTO_INCREMENT,
  `id_login` int(11) DEFAULT NULL,
  `nama_distributor` varchar(255) DEFAULT NULL,
  `p_jawab` varchar(255) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `telpon` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `rekening` varchar(255) DEFAULT NULL,
  `j_kelamin` enum('LAKI-LAKI','PEREMPUAN') DEFAULT NULL,
  `stok` int(11) DEFAULT NULL,
  `id_kabupaten` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_distributor`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

insert  into `distributor`(`id_distributor`,`id_login`,`nama_distributor`,`p_jawab`,`alamat`,`telpon`,`email`,`rekening`,`j_kelamin`,`stok`,`id_kabupaten`) values 
(1,1,'AGUS','AGUS','JLN MAJU 45','12345678','APAPUN@GMAIL.COM','345678876','LAKI-LAKI',0,1);

CREATE TABLE `kabupaten` (
  `id_kabupaten` int(11) NOT NULL AUTO_INCREMENT,
  `id_provinsi` int(11) DEFAULT NULL,
  `nama_kabupaten` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_kabupaten`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

insert  into `kabupaten`(`id_kabupaten`,`id_provinsi`,`nama_kabupaten`) values 
(1,1,'Lampung Selatan');


CREATE TABLE `kecamatan` (
  `id_kecamatan` int(11) NOT NULL AUTO_INCREMENT,
  `id_kabupaten` int(11) DEFAULT NULL,
  `nama_kecamatan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_kecamatan`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

insert  into `kecamatan`(`id_kecamatan`,`id_kabupaten`,`nama_kecamatan`) values 
(1,1,'Bakauheni');

CREATE TABLE `login` (
  `id_login` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `status` enum('PENGECER','DISTRIBUTOR','PENGAWAS') DEFAULT NULL,
  PRIMARY KEY (`id_login`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

insert  into `login`(`id_login`,`username`,`password`,`status`) values 
(1,'admin','admin','DISTRIBUTOR'),
(2,'pengecer','123456','PENGECER'),
(3,'SUSU','12123123','DISTRIBUTOR');


CREATE TABLE `pembayaran` (
  `id_pembayaran` int(11) NOT NULL AUTO_INCREMENT,
  `bukti` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_pembayaran`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

insert  into `pembayaran`(`id_pembayaran`,`bukti`) values 
(1,'jdsvjdf'),
(2,'fwefe'),
(3,'23432re32d');

CREATE TABLE `pengawas` (
  `id_pengawas` int(11) NOT NULL AUTO_INCREMENT,
  `id_login` int(11) DEFAULT NULL,
  `nama_pengawas` varchar(255) DEFAULT NULL,
  `jabatan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_pengawas`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

insert  into `pengawas`(`id_pengawas`,`id_login`,`nama_pengawas`,`jabatan`) values 
(1,1,'AGUS','SPV');

CREATE TABLE `pengecer` (
  `id_pengecer` int(11) NOT NULL AUTO_INCREMENT,
  `id_login` int(11) DEFAULT NULL,
  `nama_pengecer` varchar(255) DEFAULT NULL,
  `p_jawab` varchar(255) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `telpon` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `j_kelamin` enum('LAKI-LAKI','PEREMPUAN') DEFAULT NULL,
  `id_kecamatan` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_pengecer`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

insert  into `pengecer`(`id_pengecer`,`id_login`,`nama_pengecer`,`p_jawab`,`alamat`,`telpon`,`email`,`j_kelamin`,`id_kecamatan`) values 
(1,1,'AGUS','AGUS','JLN MAJU 45','11234456','APAPUN@GMAIL.COM','LAKI-LAKI',1);

CREATE TABLE `provinsi` (
  `id_provinsi` int(11) NOT NULL AUTO_INCREMENT,
  `nama_provinsi` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_provinsi`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;


insert  into `provinsi`(`id_provinsi`,`nama_provinsi`) values 
(1,'Lampung');

CREATE TABLE `status` (
  `id_status` int(11) NOT NULL AUTO_INCREMENT,
  `status_transaksi` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_status`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

insert  into `status`(`id_status`,`status_transaksi`) values 
(1,'BERHASIL'),
(2,'GAGAL'),
(3,'PENDING');

CREATE TABLE `transaksi` (
  `id_transaksi` int(11) NOT NULL AUTO_INCREMENT,
  `do` varchar(255) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `total_harga` int(11) DEFAULT NULL,
  `id_status` int(11) DEFAULT NULL,
  `id_distributor` int(11) DEFAULT NULL,
  `id_pengecer` int(11) DEFAULT NULL,
  `id_pembayaran` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_transaksi`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

insert  into `transaksi`(`id_transaksi`,`do`,`tanggal`,`total_harga`,`id_status`,`id_distributor`,`id_pengecer`,`id_pembayaran`) values 
(11,'w43r','2020-08-31 11:46:17',NULL,NULL,1,1,NULL),
(12,'235235235','2020-08-31 11:55:58',600000,NULL,1,1,NULL);

DROP TABLE IF EXISTS `transaksi_detail`;

CREATE TABLE `transaksi_detail` (
  `id_transaksi_detail` int(11) NOT NULL AUTO_INCREMENT,
  `id_transaksi` int(11) DEFAULT NULL,
  `nama_pupuk` varchar(255) DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `harga` int(11) DEFAULT NULL,
  `subtotal` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_transaksi_detail`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

insert  into `transaksi_detail`(`id_transaksi_detail`,`id_transaksi`,`nama_pupuk`,`jumlah`,`harga`,`subtotal`) values 
(1,11,'UREA',10,100000,1000000),
(2,11,'NSP',1,100000,100000),
(3,11,'ORGANIK',2,100000,200000),
(4,12,'UREA',1,100000,100000),
(5,12,'NPK',2,200000,400000),
(18,12,'ORGANIK',1,100000,100000);