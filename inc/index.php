<?php
session_start();
date_default_timezone_set('Asia/Jakarta');
require_once('inc/config.php');
require_once('inc/publicfunc.php');
require_once('inc/check.php');

if (isset($_POST['tambah_karyawan_post']) or isset($_POST['edit_karyawan_post'])){
	if (isset($_POST['tambah_karyawan_post'])) $tambah_karyawan_post=true;
	if (isset($_POST['edit_karyawan_post'])) $edit_karyawan_post=true;
	if (isset($_POST['id_karyawan'])) $id_karyawan=$_POST['id_karyawan'];
	$nama_karyawan=$_POST['nama_karyawan'];
	$username=$_POST['username'];
	$password=$_POST['password'];
	$opsi=$_POST['opsi'];
	$imei=$_POST['imei'];
	$status=$_POST['status'];
}
if (isset($_POST['tambah_no_hp_post']) or isset($_POST['edit_no_hp_post'])){
	if (isset($_POST['tambah_no_hp_post'])) $tambah_no_hp_post=true;
	if (isset($_POST['edit_no_hp_post'])) $edit_no_hp_post=true;
	if (isset($_POST['id_karyawan_hp'])) $id_karyawan_hp=$_POST['id_karyawan_hp'];
	$no_hp=$_POST['no_hp'];
}

if (isset($_POST['tambah_inventaris_post']) or isset($_POST['edit_inventaris_post'])){
	if (isset($_POST['tambah_inventaris_post'])) $tambah_inventaris_post=true;
	if (isset($_POST['edit_inventaris_post'])) $edit_inventaris_post=true;
	if (isset($_POST['id_inventaris'])) $id_inventaris=$_POST['id_inventaris'];
	$nama_barang=$_POST['nama_barang'];
	$tanggal=$_POST['tanggal'];
	$nilai=$_POST['nilai'];
	$penyusutan=$_POST['penyusutan'];
	$status=$_POST['status'];
}

if (isset($_POST['tambah_bank_post']) or isset($_POST['edit_bank_post'])){
	if (isset($_POST['tambah_bank_post'])) $tambah_bank_post=true;
	if (isset($_POST['edit_bank_post'])) $edit_bank_post=true;
	if (isset($_POST['id_bank'])) $id_bank=$_POST['id_bank'];
	$nama_bank=$_POST['nama_bank'];
	$no_rekening=$_POST['no_rekening'];
	$opsi=$_POST['opsi'];
	$status=$_POST['status'];
}

if (isset($_POST['tambah_komponen_kas_kecil_post']) or isset($_POST['edit_komponen_kas_kecil_post'])){
	if (isset($_POST['tambah_komponen_kas_kecil_post'])) $tambah_komponen_kas_kecil_post=true;
	if (isset($_POST['edit_komponen_kas_kecil_post'])) $edit_komponen_kas_kecil_post=true;
	if (isset($_POST['id_komponen_kas'])) $id_komponen_kas=$_POST['id_komponen_kas'];
	$nama_komponen=strtoupper($_POST['nama_komponen']);
	$jenis=$_POST['jenis'];
	$status=$_POST['status'];
}

if (isset($_POST['tambah_komponen_kas_bank_post']) or isset($_POST['edit_komponen_kas_bank_post'])){
	if (isset($_POST['tambah_komponen_kas_bank_post'])) $tambah_komponen_kas_bank_post=true;
	if (isset($_POST['edit_komponen_kas_bank_post'])) $edit_komponen_kas_bank_post=true;
	if (isset($_POST['id_komponen_bank'])) $id_komponen_bank=$_POST['id_komponen_bank'];
	$nama_komponen=$_POST['nama_komponen'];
	$jenis=$_POST['jenis'];
	$status=$_POST['status'];
}

if (isset($_POST['tambah_potongan_post']) or isset($_POST['edit_potongan_post'])){
	if (isset($_POST['tambah_potongan_post'])) $tambah_potongan_post=true;
	if (isset($_POST['edit_potongan_post'])) $edit_potongan_post=true;
	if (isset($_POST['id_potongan'])) $id_potongan=$_POST['id_potongan'];
	$nama=$_POST['nama'];
	$status=$_POST['status'];
}

if (isset($_POST['tambah_materai_post']) or isset($_POST['edit_materai_post'])){
	if (isset($_POST['tambah_materai_post'])) $tambah_materai_post=true;
	if (isset($_POST['edit_materai_post'])) $edit_materai_post=true;
	if (isset($_POST['id_materai'])) $id_materai=$_POST['id_materai'];
	$nama_materai=$_POST['nama_materai'];
	$status=$_POST['status'];
}

if (isset($_POST['tambah_pph_post']) or isset($_POST['edit_pph_post'])){
	if (isset($_POST['tambah_pph_post'])) $tambah_pph_post=true;
	if (isset($_POST['edit_pph_post'])) $edit_pph_post=true;
	if (isset($_POST['id_pph'])) $id_pph=$_POST['id_pph'];
	$nama_pph=$_POST['nama_pph'];
	$status=$_POST['status'];
}
if (isset($_POST['tambah_saldo_akhir_bank_post']) or isset($_POST['edit_saldo_akhir_bank_post'])){
	if (isset($_POST['tambah_saldo_akhir_bank_post'])) $tambah_saldo_akhir_bank_post=true;
	if (isset($_POST['edit_saldo_akhir_bank_post'])) $edit_saldo_akhir_bank_post=true;
	if (isset($_POST['id_saldo_bank'])) $id_saldo_bank=$_POST['id_saldo_bank'];
	$saldo_akhir=$_POST['saldo_akhir'];
	if (isset($_POST['tanggal'])) $tanggal=$_POST['tanggal'];
}
if (isset($_POST['tambah_gaji_post']) or isset($_POST['edit_gaji_post'])){
	if (isset($_POST['tambah_gaji_post'])) $tambah_gaji_post=true;
	if (isset($_POST['edit_gaji_post'])) $edit_gaji_post=true;
	if (isset($_POST['id_biaya_karyawan'])) $id_biaya_karyawan=$_POST['id_biaya_karyawan'];
	if (isset($_POST['id_karyawan'])) $id_karyawan=$_POST['id_karyawan'];
	$jumlah=$_POST['jumlah'];
	$tgl=explode("-", $_POST['tanggal']);
	$tanggal=$tgl[2] .'-'. $tgl[1] .'-'. $tgl[0];
}
if (isset($_POST['tambah_fee_post']) or isset($_POST['edit_fee_post'])){
	if (isset($_POST['tambah_fee_post'])) $tambah_fee_post=true;
	if (isset($_POST['edit_fee_post'])) $edit_fee_post=true;
	if (isset($_POST['id_fee'])) $id_fee=$_POST['id_fee'];
	$jumlah_fee=$_POST['jumlah_fee'];
	$status=$_POST['status'];
}
if (isset($_POST['tambah_saldo_awal_kas_post']) or isset($_POST['edit_saldo_awal_kas_post'])){
	if (isset($_POST['tambah_saldo_awal_kas_post'])) $tambah_saldo_awal_kas_post=true;
	if (isset($_POST['edit_saldo_awal_kas_post'])) $edit_saldo_awal_kas_post=true;
	if (isset($_POST['id_saldo_kas'])) $id_saldo_kas=$_POST['id_saldo_kas'];
	$tanggal=date("Y-m-d");
	$id_karyawan=$_POST['id_karyawan'];
	$saldo_awal=$_POST['saldo_awal'];
	$saldo_akhir=$_POST['saldo_akhir'];
}
if (isset($_POST['tambah_nota_masuk_post'])){
	if (isset($_POST['tambah_nota_masuk_post'])) $tambah_nota_masuk_post=true;
	if (isset($_POST['id_nota_masuk'])) $id_nota_masuk=$_POST['id_nota_masuk'];
	$tanggal_nota=date("Y-m-d");
	$nama_perusahaan=strtoupper($_POST['nama_perusahaan']);
	$nama_supplier=strtoupper($_POST['nama_supplier']);
	$no_plat=strtoupper($_POST['no_plat']);
	$berat_bersih=$_POST['berat_bersih'];
	$harga_beli=$_POST['harga_beli'];
	$total_beli=$_POST['total_beli'];
	$fee=$_POST['fee'];
	$harga_jual=$_POST['harga_jual'];
	$pph_persen=$_POST['pph_persen'];
	$pph_rp=$_POST['pph_rp'];
	$total_jual_seb_pph=$_POST['total_jual_seb_pph'];
	$total_jual_set_pph=$_POST['total_jual_set_pph'];
	$jml_tagih=round($_POST['jml_tagih']);
}
if (isset($_POST['buat_penagihan_post'])){
	if (isset($_POST['buat_penagihan_post'])) $buat_penagihan_post=true;
	$tanggal=date("Y-m-d");
	$total_tagih=$_POST['total_tagih'];
	$id_nota_masuk=$_POST['id_nota_masuk'];
}
if (isset($_POST['terima_pembayaran_post'])){
	if (isset($_POST['terima_pembayaran_post'])) $terima_pembayaran_post=true;
	$tanggal_bayar=date("Y-m-d");
	$jenis_bayar=$_POST['jenis_bayar'];
	$nama_bank=$_POST['nama_bank'];
	$materai=$_POST['materai'];
	$jumlah_bayar=$_POST['jumlah_bayar'];
}
if (isset($_POST['batal_pembayaran_post'])){
	if (isset($_POST['batal_pembayaran_post'])) $batal_pembayaran_post=true;
	if (isset($_POST['id_nota_bayar'])) $id_nota_bayar=$_POST['id_nota_bayar'];
	if (isset($_POST['id_nota_tagih'])) $id_nota_tagih=$_POST['id_nota_tagih'];
}
if (isset($_POST['tambah_kas_kecil_post'])){
	if (isset($_POST['tambah_kas_kecil_post'])) $tambah_kas_kecil_post=true;
	$tanggal=date("Y-m-d");
	$jenis=$_POST['jenis'];
	$komponen=$_POST['komponen'];
	$keterangan=$_POST['keterangan'];
	$harga=$_POST['harga'];
}
if (isset($_POST['tambah_pendapatan_bunga_post'])){
	if (isset($_POST['tambah_pendapatan_bunga_post'])) $tambah_pendapatan_bunga_post=true;
	$tanggal=$_POST['tanggal'];
	$bunga=$_POST['bunga'];
	$id_bank=$_POST['id_bank'];
}
if (isset($_POST['tambah_hutang_piutang_post'])){
	if (isset($_POST['tambah_hutang_piutang_post'])) $tambah_hutang_piutang_post=true;
	$tanggal=$_POST['tanggal'];
	$nama=strtoupper($_POST['nama']);
	$jumlah=$_POST['jumlah'];
}
if (isset($_POST['bayar_hutang_piutang_post'])){
	if (isset($_POST['bayar_hutang_piutang_post'])) $bayar_hutang_piutang_post=true;
	$id_hutang_piutang=$_POST['id_hutang_piutang'];
	$tanggal=$_POST['tanggal'];
	$jumlah=$_POST['jumlah'];
	$sisa=$_POST['sisa'];
	$bayar=$_POST['bayar'];
	$status=$_POST['status'];
}

if (isset($_POST['tambah_komponen_neraca_post'])){
	if (isset($_POST['tambah_komponen_neraca_post'])) $tambah_komponen_neraca_post=true;
	$periode=$_POST['periode'];
	$selisih_aktiva_pasiva=$_POST['selisih_aktiva_pasiva'];
	$selisih_kas=$_POST['selisih_kas'];
	$penyusutan_kas=$_POST['penyusutan_kas'];
	$modal=$_POST['modal'];
	$laba_ditahan=$_POST['laba_ditahan'];
	$laba_rugi_tahun_berjalan=$_POST['laba_rugi_tahun_berjalan'];
}
if (isset($_POST['edit_komponen_neraca_post'])){
	if (isset($_POST['edit_komponen_neraca_post'])) $edit_komponen_neraca_post=true;
	$id_komponen_neraca=$_POST['id_komponen_neraca'];
	$periode=$_POST['periode'];
	$selisih_aktiva_pasiva=$_POST['selisih_aktiva_pasiva'];
	$selisih_kas=$_POST['selisih_kas'];
	$penyusutan_kas=$_POST['penyusutan_kas'];
	$modal=$_POST['modal'];
	$laba_ditahan=$_POST['laba_ditahan'];
	$laba_rugi_tahun_berjalan=$_POST['laba_rugi_tahun_berjalan'];
}
if (isset($_POST['batal_tagihan_post'])){
	if (isset($_POST['batal_tagihan_post'])) $batal_tagihan_post=true;
	if (isset($_POST['id_nota_bayar'])) $id_nota_bayar=$_POST['id_nota_bayar'];
	if (isset($_POST['id_nota_tagih'])) $id_nota_tagih=$_POST['id_nota_tagih'];
}

if (isset($_GET['id'])){
	$id=$_GET['id'];
}

if(isset($_GET['page'])){
	$mod = $_GET['mod'];
	$page = $_GET['page'];
} else {
	$mod="dashboard";
	$page="dashboard";
}
if (isset($_SESSION['pesan'])){
	$pesan=$_SESSION['pesan'];
	$warna=$_SESSION['warna'];
	unset($_SESSION['pesan']);
	unset($_SESSION['warna']);
}

include "inc/header.php";
include "inc/top.php";
include "inc/sidebar.php";

include "pages/" .$mod. "/" .$page. ".php";
include "inc/footer.php";

?>