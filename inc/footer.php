 
  <footer class="main-footer">
    <strong>Copyright THE PUPUK.</strong> All rights
    reserved.
  </footer>

</div>
<!-- ./wrapper -->

<!-- Bootstrap 3.3.7 -->
<script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Date Picker -->
<script src="vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- daterangepicker -->
<script src="vendors/moment/min/moment.min.js"></script>
<script src="vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- jQuery Input Mask-->
<script src="vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
<!-- DataTable -->
	<script src="vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="vendors/datatables.net-bs/js/dataTables.bootstrap.js"></script>
<!-- Select2 -->
	<script src="vendors/select2/dist/js/select2.full.min.js"></script>
<!-- AdminLTE App -->
<script src="assets/js/adminlte.min.js"></script>
<script src="assets/js/pupuk.js"></script>
</body>
</html>
