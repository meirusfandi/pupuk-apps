
<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
<?php
echo '	<li class="treeview ' .($mod=='master' ? ' active menu-open' : ''). '">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>MASTER</span>
			<span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li ' .($mod=='master' && $page=='login' ? ' class="active"' : ''). ' style="padding-left:20px"><a href="?mod=master&page=login"><i class="fa fa-file-o"></i> Login</a></li>
            <li ' .($mod=='master' && $page=='distributor' ? ' class="active"' : ''). ' style="padding-left:20px"><a href="?mod=master&page=distributor"><i class="fa fa-file-o"></i> Distributor</a></li>
            <li ' .($mod=='master' && $page=='pengawas' ? ' class="active"' : ''). ' style="padding-left:20px"><a href="?mod=master&page=pengawas"><i class="fa fa-file-o"></i> Pengawas</a></li>
			<li ' .($mod=='master' && $page=='pengecer' ? ' class="active"' : ''). ' style="padding-left:20px"><a href="?mod=master&page=pengecer"><i class="fa fa-file-o"></i> Pengecer</a></li>
			<li ' .($mod=='master' && $page=='status' ? ' class="active"' : ''). ' style="padding-left:20px"><a href="?mod=master&page=status"><i class="fa fa-file-o"></i> Status Transaksi</a></li>
			<li ' .($mod=='master' && $page=='dist_has_pupuk' ? ' class="active"' : ''). ' style="padding-left:20px"><a href="?mod=master&page=dist_has_pupuk"><i class="fa fa-file-o"></i> Dist Has Pupuk</a></li>
			<li ' .($mod=='master' && $page=='provinsi' ? ' class="active"' : ''). ' style="padding-left:20px"><a href="?mod=master&page=provinsi"><i class="fa fa-file-o"></i> Provinsi</a></li>
			<li ' .($mod=='master' && $page=='kabupaten' ? ' class="active"' : ''). ' style="padding-left:20px"><a href="?mod=master&page=kabupaten"><i class="fa fa-file-o"></i> Kabupaten</a></li>
			<li ' .($mod=='master' && $page=='kecamatan' ? ' class="active"' : ''). ' style="padding-left:20px"><a href="?mod=master&page=kecamatan"><i class="fa fa-file-o"></i> Kecamatan</a></li>
          </ul>
        </li>';
echo '	<li class="treeview ' .($mod=='transaksi' ? ' active menu-open' : ''). '">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>TRANSAKSI</span>
			<span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li ' .($mod=='transaksi' && $page=='view' ? ' class="active"' : ''). ' style="padding-left:20px"><a href="?mod=transaksi&page=view"><i class="fa fa-file-o"></i> View</a></li>
          </ul>
        </li>';
?>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>