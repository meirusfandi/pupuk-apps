<?php
	if (!isset($_SESSION['user_pupuk'])) {
		_direct("login.php");
		exit();
	}
	if(time() - $_SESSION['timestamp_pupuk'] > 60*60*4) {
		session_destroy();
		header("Location: login.php");
		exit;
	} else {
		$_SESSION['timestamp_pupuk'] = time();
	}
?>